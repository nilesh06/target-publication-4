<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('employees', function (Blueprint $table) {
			$table->id();
			$table->string('name');
			$table->string('address_line_1');
			$table->string('address_line_2')->nullable();
			$table->string('location')->nullable();
			$table->string('zip_code')->nullable();
			$table->string('postal_area')->nullable();
			$table->string('taluka')->nullable();
			$table->string('suburb')->nullable();
			$table->string('direction')->nullable();
			$table->string('city');
			$table->string('distict')->nullable();
			$table->string('state')->nullable();
			$table->string('country')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('employees');
	}
}
