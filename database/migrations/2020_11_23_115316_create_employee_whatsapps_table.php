<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeWhatsappsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('employee_whatsapps', function (Blueprint $table) {
			$table->id();
			$table->integer('employee_id');
			$table->string('whatsapp_mobile');
			$table->tinyInteger('is_primary')->default(1)->comment('0=>primary,1=>primary');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('employee_whatsapps');
	}
}
