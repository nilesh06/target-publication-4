<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeEmail extends Model {
	protected $fillable = [
		'employee_id',
		'email',
		'is_primary',
	];
}
