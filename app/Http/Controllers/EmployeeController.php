<?php

namespace App\Http\Controllers;

use App\Employee;
use App\EmployeeEmail;
use App\EmployeeMobile;
use App\EmployeeWhatsapp;
use App\State;
use Illuminate\Http\Request;

class EmployeeController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$employees = Employee::all();
		return view('employee.index', compact('employees'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$states = State::all();
		return view('employee.create', compact('states'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$this->validate($request, [
			'name' => 'required',
			'address_line_1' => 'required',
			'city' => 'required',
		]);

		$employee = Employee::create($request->all());

		foreach ($request->mobile as $key => $value) {
			if ($value != null) {
				EmployeeMobile::create([
					'employee_id' => $employee->id,
					'mobile' => $value,
				]);
			}
		}

		foreach ($request->email as $key => $value) {
			if ($value != null) {
				EmployeeEmail::create([
					'employee_id' => $employee->id,
					'email' => $value,
				]);
			}
		}

		foreach ($request->whatsapp_mobile as $key => $value) {
			if ($value != null) {
				EmployeeWhatsapp::create([
					'employee_id' => $employee->id,
					'whatsapp_mobile' => $value,
				]);
			}
		}

		return redirect()->route('employee.index')->with('success', 'Employee added successfully');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Employee  $employee
	 * @return \Illuminate\Http\Response
	 */
	public function show(Employee $employee) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Employee  $employee
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Employee $employee) {
		$states = State::all();
		return view('employee.edit', compact('states', 'employee'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Employee  $employee
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Employee $employee) {
		$this->validate($request, [
			'name' => 'required',
			'address_line_1' => 'required',
			'city' => 'required',
		]);

		$employee->update($request->all());

		$mobile_id_array = array();
		$email_id_array = array();
		$whatsapp_mobile_id_array = array();

		$old_mobile_ids = $employee->mobile()->pluck('id')->toArray();
		$old_whatsapp_ids = $employee->whatsapp()->pluck('id')->toArray();
		$old_email_ids = $employee->email()->pluck('id')->toArray();

		if (count($request->mobile) > 0) {
			foreach ($request->mobile as $key => $value) {
				if ($value != null) {
					if ($request->mobile_id[$key] == null) {
						EmployeeMobile::create([
							'employee_id' => $employee->id,
							'mobile' => $value,
						]);
					} else {
						EmployeeMobile::where('id', $request->mobile_id[$key])->update([
							'mobile' => $value,
						]);
						array_push($mobile_id_array, $request->mobile_id[$key]);
					}
				}
			}
		}

		foreach ($request->email as $key => $value) {
			if ($value != null) {
				if ($request->email_id[$key] == null) {
					EmployeeEmail::create([
						'employee_id' => $employee->id,
						'email' => $value,
					]);
				} else {
					EmployeeEmail::where('id', $request->email_id[$key])->update([
						'email' => $value,
					]);
					array_push($email_id_array, $request->email_id[$key]);
				}
			}
		}

		foreach ($request->whatsapp_mobile as $key => $value) {
			if ($value != null) {
				if ($request->whatsapp_mobile_id[$key] == null) {
					EmployeeWhatsapp::create([
						'employee_id' => $employee->id,
						'whatsapp_mobile' => $value,
					]);
				} else {
					EmployeeWhatsapp::where('id', $request->whatsapp_mobile_id[$key])->update([
						'whatsapp_mobile' => $value,
					]);
					array_push($whatsapp_mobile_id_array, $request->whatsapp_mobile_id[$key]);
				}
			}
		}

		foreach ($old_mobile_ids as $key => $value) {
			if (!in_array($value, $mobile_id_array)) {
				EmployeeMobile::where('id', $value)->delete();
			}
		}

		foreach ($old_whatsapp_ids as $key => $value) {
			if (!in_array($value, $whatsapp_mobile_id_array)) {
				EmployeeWhatsapp::where('id', $value)->delete();
			}
		}

		foreach ($old_email_ids as $key => $value) {
			if (!in_array($value, $email_id_array)) {
				EmployeeEmail::where('id', $value)->delete();
			}
		}

		return redirect()->route('employee.index')->with('success', 'Employee updated successfully');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Employee  $employee
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Employee $employee) {
		$employee->mobile()->delete();
		$employee->whatsapp()->delete();
		$employee->email()->delete();
		$employee->delete();

		return ['success'];
	}
}
