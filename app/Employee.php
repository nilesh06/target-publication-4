<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model {
	protected $fillable = [
		'name',
		'address_line_1',
		'address_line_2',
		'location',
		'zip_code',
		'postal_area',
		'taluka',
		'suburb',
		'direction',
		'city',
		'distict',
		'state',
		'country',
	];

	public function mobile() {
		return $this->hasMany(EmployeeMobile::class, 'employee_id');
	}

	public function whatsapp() {
		return $this->hasMany(EmployeeWhatsapp::class, 'employee_id');
	}

	public function email() {
		return $this->hasMany(EmployeeEmail::class, 'employee_id');
	}
}
