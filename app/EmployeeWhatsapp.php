<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeWhatsapp extends Model {
	protected $fillable = [
		'employee_id',
		'whatsapp_mobile',
		'is_primary',
	];
}
