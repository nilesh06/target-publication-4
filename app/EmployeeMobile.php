<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeMobile extends Model {
	protected $fillable = [
		'employee_id',
		'mobile',
		'is_primary',
	];
}
