@extends('layouts.master')

@section('page-content')
	<div class="container">
		<div class="row">
				<div class="col-md-12">
					<a href="{{ route('employee.create') }}" style="float: right;">
						<button class="btn btn-success text-right">Add Employee</button>
					</a>
					<h2>Employee List</h2>
				</div>
			</div>
		<div class="row">
			<div class="col-md-12">
				@if (session('success'))
                	<div class="alert alert-success alert-dismissible mb-2" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
						{{ session('success') }}
					</div>
				@endif
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($employees as $employee)
							<tr>
								<td>{{$loop->iteration}}</td>
								<td>{{$employee->name}}</td>
								<td>
									<a href="{{ route('employee.edit',$employee->id) }}">
										<button class="btn btn-success">
											<i class="fa fa-pencil"></i>
										</button>
									</a>

									<a href="#">
										<button class="btn btn-danger delete" data-id="{{$employee->id}}">
											<i class="fa fa-trash"></i>
										</button>
									</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection


@section('page-script')
	<script type="text/javascript">
		$(document).ready(function(){
			$(document).on('click','.delete',function(){
				var id = $(this).attr('data-id');
				var r = confirm("Are You Sure?");
	            if (r == true)
	            {
    	            $.ajax({
		                url : '/employee/'+id,
		                type : 'post',
		                dataType: "json",
		                data: {
                            "_token": "{{ csrf_token() }}",
                            "_method":'DELETE',
                            "id":id,
                        },
		                success : function(res){
		                    console.log(res);
		                    if(res == 'success')
		                    {
		                    	alert('Employee deleted successfully');
		                    	location.reload();
		                    }
		                    else
		                    {
		                    	alert('something went worng');
		                    }
		                }
		            })
	            }
	            else
	            {
	                alert("Your employee is Safe. :)")
	            }
			})
		})
	</script>
@endsection