@extends('layouts.master')

@section('page-content')
	<div class="container">
		<form action="{{ route('employee.update',$employee->id) }}" method="post">
			@csrf
			{{ method_field('PUT') }}
			<div class="row">
				<div class="col-md-12">
					<h2>Employee Details</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label>Employee Name<sup><i class="fa fa-certificate text-danger"></i></sup></label>
						<input type="text" name="name" id="name" class="form-control" required="" placeholder="Enter Name" value="{{$employee->name}}">
						@error('name')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h2>Address Details</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label>Address 1<sup><i class="fa fa-certificate text-danger"></i></sup></label>
						<input type="text" name="address_line_1" id="address_line_1" class="form-control"  required="" placeholder="Enter Address 1" value="{{$employee->address_line_1}}">
						@error('address_line_1')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label>Address 2</label>
						<input type="text" name="address_line_2" id="address_line_2" class="form-control"  placeholder="Enter Address 2" value="{{$employee->address_line_2}}">
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label>Location</label>
						<input type="text" name="location" id="location" class="form-control"  placeholder="Enter Address 2" value="{{$employee->location}}">
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label>Zip/Pincode</label>
						<input type="text" name="zip_code" id="zip_code" class="form-control"  placeholder="Enter Zip/Pincode" value="{{$employee->zip_code}}">
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label>Postal Area</label>
						<input type="text" name="postal_area" id="postal_area" class="form-control"  placeholder="Enter Postal Area" value="{{$employee->postal_area}}">
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label>Taluka</label>
						<input type="text" name="taluka" id="taluka" class="form-control"  placeholder="Enter Taluka" value="{{$employee->taluka}}">
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label>Suburb</label>
						<input type="text" name="suburb" id="suburb" class="form-control"  placeholder="Enter Taluka" value="{{$employee->suburb}}">
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label>East/West</label>
						<input type="text" name="direction" id="direction" class="form-control"  placeholder="Enter East/West" value="{{$employee->direction}}">
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label>City <sup><i class="fa fa-certificate text-danger"></i></sup></label>
						<input type="text" name="city" id="city" class="form-control" required="" placeholder="Enter East/West"value="{{$employee->city}}">
						@error('city')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label>Distict</label>
						<input type="text" name="distict" id="distict" class="form-control"  placeholder="Enter Distict"value="{{$employee->distict}}">
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label>State</label>
						<select class="form-control" name="state" id="state">
							<option value="">-- Select State --</option>
							@foreach($states as $state)
								<option value="{{$state->state_name}}" @if($state->state_name == 'MAHARASHTRA') selected @endif>{{$state->state_name}}</option>
							@endforeach
						</select>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label>County</label>
						<select class="form-control" name="country" id="country">
							<option value="">-- Select Country --</option>
							<option value="India" selected="">India</option>
							<option value="USA">USA</option>
							<option value="UK">UK</option>
							<option value="Shri Lanka">Shri Lanka</option>
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h2>Contact Details</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="col-md-12">
						<div>
							<button type="button" class="btn btn-default" id="add_mobile" style="float: right;text-align: right;"><i class="fa fa-plus"></i></button>
							<label>Mobile Numbers </label>
						</div>
						@if(count($employee->mobile) > 0)
							@foreach($employee->mobile as $mobile)
								<div class="form-group">
									<button type="button" class="btn btn-default remove_mobile"><i class="fa fa-trash"></i></button>
									<!-- <label style="float:right;text-align:right"><input type="radio" name="primary_mobile[]" checked=""> Primary?</label> -->
									<input type="text" name="mobile[]" class="form-control"  placeholder="Enter Mobile number" value="{{$mobile->mobile}}">
									<input type="hidden" name="mobile_id[]" value="{{$mobile->id}}">
								</div>
							@endforeach
						@else
							<div class="form-group">
								<label>
									Mobile
								</label>
								<!-- <label style="float:right;text-align:right"><input type="radio" name="primary_mobile[]" checked=""> Primary?</label> -->
								<input type="text" name="mobile[]" class="form-control"  placeholder="Enter Mobile number">
								<input type="hidden" name="mobile_id[]">
							</div>
						@endif
						<div id="append_mobile"></div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="col-md-12">
						<div>
							<button type="button" class="btn btn-default" id="add_whatsapp" style="float: right;text-align: right;"><i class="fa fa-plus"></i></button>
							<label>Whatsapp Numbers</label>
						</div>
						@if(count($employee->whatsapp) > 0)
							@foreach($employee->whatsapp as $whatsapp)
								<div class="form-group">
									<button type="button" class="btn btn-default remove_whatsapp"><i class="fa fa-trash"></i></button>
									<input type="text" name="whatsapp_mobile[]" class="form-control"  placeholder="Enter Whatsapp number" value="{{$whatsapp->whatsapp_mobile}}">
									<input type="hidden" name="whatsapp_mobile_id[]" value="{{$whatsapp->id}}">
								</div>
							@endforeach
						@else
							<div class="form-group">
								<!-- <label style="float:right;text-align:right">
									<input type="radio" name="primary_whatsapp[]"  checked=""> Primary?
								</label> -->
								<label>Whatsapp Number</label>
								<input type="text" name="whatsapp_mobile[]" class="form-control"  placeholder="Enter Whatsapp number">
								<input type="hidden" name="whatsapp_mobile_id[]">
							</div>
						@endif
						<div id="append_whatsapp"></div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="col-md-12">
						<div>
							<button type="button" class="btn btn-default" id="add_email" style="float: right;text-align: right;"><i class="fa fa-plus"></i></button>
							<label>Email Addresses</label>
						</div>
						@if(count($employee->email) > 0)
							@foreach($employee->email as $email)
								<div class="form-group">
									<button type="button" class="btn btn-default remove_email"><i class="fa fa-trash"></i></button>
									<input type="email" name="email[]" class="form-control" required=""  placeholder="Enter Email" value="{{$email->email}}">
									<input type="hidden" name="email_id[]" value="{{$email->id}}">
								</div>
							@endforeach
						@else
							<div class="form-group">
								<!-- <label style="float:right;text-align:right">
										<input type="radio" name="primary_email[]" checked=""> Primary?
									</label> -->
								<label>Email<sup><i class="fa fa-certificate text-danger"></i></sup></label>
								<input type="email" name="email[]" class="form-control" required=""  placeholder="Enter Email">
								<input type="hidden" name="email_id[]">
							</div>
						@endif
						<div id="append_email"></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12" >
					<button type="submit" class="btn btn-success">Save</button>
				</div>
			</div>
		</form>
	</div>
@endsection

@section('page-script')
	<script type="text/javascript">
		$(document).on('click','#add_mobile',function(){
			$('#append_mobile').append(`
				<div class="form-group">
					<button type="button" class="btn btn-default remove_mobile"><i class="fa fa-trash"></i></button>
					<input type="text" name="mobile[]" class="form-control"  placeholder="Enter Mobile number">
					<input type="hidden" name="mobile_id[]">
				</div>
			`)
		})

		$(document).on('click','.remove_mobile', function(){
			$(this).parent().remove();
		});

		$(document).on('click','#add_whatsapp',function(){
			$('#append_whatsapp').append(`
				<div class="form-group">
					<button type="button" class="btn btn-default remove_whatsapp"><i class="fa fa-trash"></i></button>
					<input type="text" name="whatsapp_mobile[]" class="form-control"  placeholder="Enter Whatsapp number">
					<input type="hidden" name="whatsapp_mobile_id[]">
				</div>
			`)
		})

		$(document).on('click','.remove_whatsapp', function(){
			$(this).parent().remove();
		});

		$(document).on('click','#add_email',function(){
			$('#append_email').append(`
				<div class="form-group">
					<button type="button" class="btn btn-default remove_email"><i class="fa fa-trash"></i></button>
					<input type="email" name="email[]" class="form-control"  placeholder="Enter Email">
					<input type="hidden" name="email_id[]">
				</div>
			`)
		})

		$(document).on('click','.remove_email', function(){
			$(this).parent().remove();
		});
	</script>
@endsection